package main

import (
	"context"
	pb "gitlab.com/Alzoww/rgateway/microservice2/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Server struct {
	pb.Microservice2Server
}

func (s *Server) SayHello(context.Context, *pb.HelloRequest) (*pb.HelloResponse, error) {
	return &pb.HelloResponse{Message: "Hello from microservice 2!"}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":50052")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	server := Server{}
	pb.RegisterMicroservice2Server(s, &server)

	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
