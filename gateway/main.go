package main

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pb1 "gitlab.com/Alzoww/rgateway/microservice1/proto"
	pb2 "gitlab.com/Alzoww/rgateway/microservice2/proto"
	"google.golang.org/grpc"
	"log"
	"net/http"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	//http://localhost:8080/1/sayhello
	err := pb1.RegisterMicroservice1HandlerFromEndpoint(ctx, mux, "microservice1:50051", opts)
	if err != nil {
		log.Fatalf("failed to start HTTP server: %v", err)
	}

	//http://localhost:8080/2/sayhello
	err = pb2.RegisterMicroservice2HandlerFromEndpoint(ctx, mux, "microservice2:50052", opts)
	if err != nil {
		log.Fatalf("failed to start HTTP server: %v", err)
	}

	log.Fatal(http.ListenAndServe(":8080", mux))

}
