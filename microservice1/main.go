package main

import (
	"context"
	pb "gitlab.com/Alzoww/rgateway/microservice1/proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type Server struct {
	pb.Microservice1Server
}

func (s *Server) SayHello(context.Context, *pb.HelloRequest) (*pb.HelloResponse, error) {
	return &pb.HelloResponse{Message: "Hello from microservice 1!"}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	server := Server{}
	pb.RegisterMicroservice1Server(s, &server)

	err = s.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
